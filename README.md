AC263 Final Project
===

## Structure

* app: Web application code and execution framework
* docs: Documentation in HTML and LaTeX (with compiled PDF) formats
* final: Final report documents
* interim: Interim status report documents
* lib: CakePHP library
* scripts: Individual test cases, pre-processing libraries, and initial language benchmarks

## Dependencies

### Packages
 
* json-c
* mysql
* mysql-server
* php
* php-mysql
* php-pdo
* php-pecl-apc
* php-PHPMailer

### Services

* httpd 
* mysqld
