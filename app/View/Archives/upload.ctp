<style>

html, body {
    background: #efefef;
    height: 100%;
}

h1 {
    margin-top: 20px;
    margin-bottom: 20px;
}

input[type=file] {
    margin-bottom: 20px;
}

</style>

<div class="content-wrapper-outer">
    <div class="content-wrapper-inner">
        <div style="display: block; margin: 0 auto; text-align: center">
            <h1 style="margin-top: 20px">Upload an archive of student submissions.</h1>
            <form action="/archives/add" method="post" enctype="multipart/form-data">
                <input type="email" name="email" required placeholder="Email" /><br />
                <input type="file" name="file" required /><br />
                <input type="submit" value="Upload" class="btn btn-large btn-primary" />
            </form>
        </div>
    </div>
</div>
