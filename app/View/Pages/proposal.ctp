<style>

html, body {
    background: #efefef;
    height: 100%;
}

h2 {
    margin-top: 30px;
}

li {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-size: 18px;
    font-weight: 200;
    line-height: 27px;
}

img {
    width: 24px;
    padding-right: 10px;
    display: inline-block;
    vertical-align: baseline;
}

</style>

<div class="content-wrapper-outer">
    <div class="content-wrapper-inner">
        <div class="hero-unit">
            <h1 style="text-align: center; margin-top: 20px; margin-bottom: 50px">Proposal</h1>
            <p>Our comparison algorithm will be run in multiple phases. Each phase of the algorithm will mark submissions as suspicious or unsuspicuous, which will increment or decrement the document's plagiarism "score". Before each phase is run, the score of each document will be compared to some threshold, where a score above or below some pre-determined value will mark the document as likely plagiarized or likely not plagiarized, which will remove the document from the inputs of subsequent phases. Because the input size for subsequent phases will likely decrease, the amount of work done by each phase can increase. After the completion of all phases, a final test will be run to determine which pairs of documents are likely plagiarized.</p>
            <p>Each phase of the algorithm can be parallelized, since the comparison of two documents is independent from other documents. In particular, this problem may be made more efficient by technologies like MapReduce.</p>
            <p>In designing the document scorer, we plan to architect a framework in which we can easily introduce or change phases. Each phase will be encapsulated and conform to a common interface, such that each phase can be swappable and developed independently of other phases. We also plan to implement de-duplication so that identical files are not duplicated on disk, as well as compress files when they are not being compared.</p>
            <h2><img src="/img/186-ruler@2x.png" /> Metrics</h2>
            <p>The phases of our comparison algorithm may include:</p>
            <ul>
                <li>Similar file size</li>
                <li>Hash comparison (to a pre-computed set of hashes)</li>
                <li>Byte-by-byte comparison</li>
                <li>Metadata comparison (e.g., correctness scores)</li>
                <li>Comparison of prettified source</li>
                <li>AST parsing and substitution of variable names (e.g., via YACC, YAXX, Bison, etc.)</li>
                <li>Document fingerprinting (e.g., Rabin, edit distance)</li>
            </ul>
            <h2><img src="/img/20-gear-2@2x.png" /> Process</h2>
            <p>We have access to students' (anonymized) CS50 submissions from past years (in addition to submissions from edX students), as well as which of those submissions were found to be plagiarized via a combination of machine and human examination. We plan to compare the effectiveness of the various metrics to see which most reliably predict plagiarized work.</p>
            <h2><img src="/img/11-clock@2x.png" /> Timeline</h2>
            <ul>
                <li>Run benchmarks using a variety of languages to determine the best technologies for our document comparison algorithm.</li>
                <li>Implement the steps of our algorithm and determine which have practical running times to use at a large scale.</li> 
                <li>Determine a database schema to record the results of comparison tests.</li>
                <li>Create a framework for batch processing.</li>
                <li>Test our algorithm on a variety of inputs and record running times.</li>
                <li>Create a web interface for uploading submission archives and receiving notifications when the comparison is complete.</li>
                <li>Allow users to visualize results to determine which files are potentially plagiarized as well as potentially identify cheating "networks," in which student A is similar to student B and student B is similar to student C.</li>
            </ul>
        </div>
    </div>
</div>
