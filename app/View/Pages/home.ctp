<style>

html, body {
    background: #efefef;
    height: 100%;
}

.box {
    border: 1px solid #ccc;
    background: white;
    border-radius: 10px;
    height: 170px;
    padding-top: 30px;
    box-shadow: rgba(0, 0, 0, 0.4) 0 1px 3px;
    -webkit-box-shadow: rgba(0, 0, 0, 0.4) 0 1px 3px;
    -moz-box-shadow: rgba(0, 0, 0, 0.4) 0 1px 3px;
}

.box img {
    margin-top: 20px;
}

</style>

<div class="content-wrapper-outer">
    <div class="content-wrapper-inner">
        <div class="hero-unit">
            <h1 style="text-align: center; margin-top: 20px">Scalable plagiarism detection.</h1>
            <div style="margin-top: 50px; text-align: center">
                <p>CS50 Compare allows instructors to identify academic dishonesty. Supply CS50 Compare with an archive of submissions, and you'll see which submissions are most similar. CS50 Compare knows that renaming variables is still cheating, and CS50 Compare is ready to handle detection at a massive scale.</p><p><strong>Read the <a href="/proposal">proposal</a> for more details.</strong></p>

            </div>
            <div class="row-fluid" style="margin-top: 50px; text-align: center">
                <div class="span4 box">
                    <h2><a href="/upload">1. Upload submissions.</a></h2>
                    <img src="/img/266-upload@2x.png" />
                </div>
                <div class="span4 box">
                    <h2>2. Sit back and relax.</h2>
                    <img src="/img/403-coffee-mug@2x.png" style="width: 47px" />
                </div>
                <div class="span4 box">
                    <h2>3. View similarities.</h2>
                    <img src="/img/362-2up@2x.png" />
                </div>
            </div>
        </div>
    </div>
</div>
