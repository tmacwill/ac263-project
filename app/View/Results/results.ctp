<?php

// avoid adding duplicate entries to select boxes
$source_select = '';
$target_select = '';
$source_added = [];
$target_added = [];

// build select boxes
foreach ($results as $source => $targets) {
    foreach ($targets as $target => $score) {
        // add file to source dropdown
        if (!isset($source_added[$source])) {
            $source_select .= "<option value=\"$source\">$source</option>";
            $source_added[$source] = 1;
        }

        // add file to target dropdown
        if (!isset($target_added[$target])) {
            $target_select .= "<option value=\"$target\">$target</option>";
            $target_added[$target] = 1;
        }
    }
}

?>

<style>

html, body {
    background: #efefef;
    height: 100%;
}

h1 {
    margin-top: 20px;
    margin-bottom: 20px;
}

th {
    cursor: pointer;
}

.sigma-parent {
    position: relative;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    background: #222;
    height: 500px;
}

.sigma-expand {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
}

#graph-parent {
    display: inline-block;
    width: 70%;
}

#graph-details {
    display: inline-block;
    width: 20%;
    position: absolute;
    margin-left: 10px;
}

#threshold-slider {
    margin-left: 5px;
}

.Differences {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    empty-cells: show;
}

.Differences thead th {
    text-align: left;
    border-bottom: 1px solid #000;
    background: #aaa;
    color: #000;
    padding: 4px;
}

.Differences tbody th {
    text-align: right;
    background: #ccc;
    width: 4em;
    padding: 1px 2px;
    border-right: 1px solid #000;
    vertical-align: top;
    font-size: 13px;
}

.Differences td {
    padding: 1px 2px;
    font-family: Consolas, monospace;
    font-size: 13px;
}

.DifferencesSideBySide .ChangeInsert td.Left {
    background: #dfd;
}

.DifferencesSideBySide .ChangeInsert td.Right {
    background: #cfc;
}

.DifferencesSideBySide .ChangeDelete td.Left {
    background: #f88;
}

.DifferencesSideBySide .ChangeDelete td.Right {
    background: #faa;
}

.DifferencesSideBySide .ChangeReplace .Left {
    background: #fe9;
}

.DifferencesSideBySide .ChangeReplace .Right {
    background: #fd8;
}

.Differences ins, .Differences del {
    text-decoration: none;
}

.DifferencesSideBySide .ChangeReplace ins, .DifferencesSideBySide .ChangeReplace del {
    background: #fc0;
}

.Differences .Skipped {
    background: #f7f7f7;
}

.DifferencesInline .ChangeReplace .Left,
.DifferencesInline .ChangeDelete .Left {
    background: #fdd;
}

.DifferencesInline .ChangeReplace .Right,
.DifferencesInline .ChangeInsert .Right {
    background: #dfd;
}

.DifferencesInline .ChangeReplace ins {
    background: #9e9;
}

.DifferencesInline .ChangeReplace del {
    background: #e99;
}

pre {
    width: 100%;
    overflow: auto;
}

</style>

<link rel="stylesheet" type="text/css" href="/css/lib/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css" href="/css/lib/io/jquery-ui-1.8.16.custom.css" />

<script type="text/javascript" src="/js/lib/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="/js/lib/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/lib/sigma.min.js"></script>
<script type="text/javascript" src="/js/uploads/results.js"></script>

<script>
var id = <?= $id ?>;
var graph = <?= json_encode($json) ?>;
</script>

<div class="content-wrapper-outer">
    <div class="content-wrapper-inner">
        <div style="display: block; margin: 0 auto; text-align: center">
            <h1 style="margin-top: 20px">Test results.</h1>
        </div>
        <div id="tabs">
            <ul>
                <li><a href="#tab-graph">Graph</a></li>
                <li><a href="#tab-table">Table</a></li>
                <li><a href="#tab-compare">Compare</a></li>
            </ul>
            <div id="tab-graph">
                <div class="sigma-parent" id="graph-parent">
                    <div class="sigma-expand" id="graph"></div>
                </div>
                <div id="graph-details">
                    <h3>Threshold: <span id="threshold-value">0</span></h3>
                    <div id="threshold-slider"></div>
                </div>
            </div>
            <div id="tab-table">
                <table id="table">
                    <thead>
                        <tr>
                            <th>Student 1</th>
                            <th>Student 2</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($results as $source => $v): ?>
                            <?php foreach ($v as $target => $score): ?>
                                <tr>
                                    <td><?= $source ?></td>
                                    <td><?= $target ?></td>
                                    <td><?= $score ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div id="tab-compare">
                <select id="source"><?= $source_select ?></select>
                <select id="target"><?= $target_select ?></select>
                <div id="diff"></div>
            </div>
        </div>
    </div>
</div>
