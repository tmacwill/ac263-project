<?php

$total_phases = 4;

?>

<style>

html, body {
    background: #efefef;
    height: 100%;
}

h1 {
    margin-top: 20px;
    margin-bottom: 20px;
}

</style>

<?php if (count($phases) < $total_phases): ?>
    <meta http-equiv="refresh" content="5">
<?php endif; ?>

<div class="content-wrapper-outer">
    <div class="content-wrapper-inner">
        <div style="display: block; margin: 0 auto; text-align: center">
            <h1 style="margin-top: 20px">Current testing status.</h1>
        </div>
        <div>
            <?php if (count($phases) == 0): ?>
                <h1>Your tests are currently processing!</h1>
            <?php else: ?>
                <ul style="list-style-type: none">
                    <?php foreach ($phases as $phase): ?>
                    <li>
                        <strong><?= $phase['Phase']['test'] ?></strong> started at <?= $phase['Phase']['start'] ?>
                        <?php if ($phase['Phase']['end']): ?> and completed at <?= $phase['Phase']['end'] ?><?php endif; ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
        
                <?php if (count($phases) == $total_phases): ?>
                    <h3 style="text-align: center"><a href="/results/<?= $id ?>">View Results</a></h3>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
