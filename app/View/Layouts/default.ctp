<!doctype html>
<html>
<head>
    <title>CS50 Compare</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <?php

        // load app-wide css
        $this->Html->css(array(
            'lib/bootstrap.min',
            'global'
        ), null, array('block' => 'libcss'));

        // load app-wide javascript
        $this->Html->script(array(
            'lib/jquery.min',
            'lib/bootstrap.min',
            'lib/underscore-min',
            'lib/backbone-min',
        ), array('block' => 'libjs'));

        echo $this->fetch('libcss');
        echo $this->fetch('css');

        echo $this->fetch('libjs');
        echo $this->fetch('script');

        echo $this->fetch('meta');

    ?>
</head>
<body>
    <?php echo $this->element('navbar'); ?>
    <?php echo $this->fetch('content'); ?>
</body>
</html>
