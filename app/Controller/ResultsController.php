<?php

App::uses('AppController', 'Controller');

class ResultsController extends AppController {
    /**
     * Load associated models
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();

        Controller::loadModel('Archive');
        Controller::loadModel('Phase');
        Controller::loadModel('Result');
    }

    /**
     * Generate a side-by-side diff of two files
     *
     * @param $id ID of archive to compare files within
     *
     */
    public function diff($id) {
        // diff library
        require_once __DIR__ . '/../Lib/diff/Diff.php';
        require_once __DIR__ . '/../Lib/diff/Diff/Renderer/Html/SideBySide.php';

        // make sure parameters are set
        if (!isset($_GET['source'], $_GET['target']))
            exit;

        // determine path to files
        $source_path = Archive::ARCHIVE_PATH . "$id/{$_GET['source']}";
        $target_path = Archive::ARCHIVE_PATH . "$id/{$_GET['target']}";
        if (!is_file($source_path) || !is_file($target_path))
            exit;

        // diff files
        $source = explode("\n", file_get_contents($source_path));
        $target = explode("\n", file_get_contents($target_path));
        $diff = new Diff($source, $target, [
            'ignoreWhitespace' => true,
            'ignoreCase' => true
        ]);

        // compute diff
        $renderer = new Diff_Renderer_Html_SideBySide;
        echo json_encode([
            'content' => $diff->Render($renderer),
            'source' => $_GET['source'],
            'target' => $_GET['target']
        ]);
        exit;
    }

    /**
     * Generate a graph for a result set
     *
     * @param $id ID of archive to generate graph for
     *
     */
    public function graph($id) {
        $graph = $this->Result->graph($id);

        echo json_encode($graph['json']);
        exit;
    }

    /**
     * View the results of the tests
     *
     * @param $id ID of upload to check status of
     *
     */
    public function results($id) {
        $graph = $this->Result->graph($id);

        $this->set('results', $graph['results']);
        $this->set('json', $graph['json']);
        $this->set('id', $id);
    }

    /**
     * Check the status of an upload
     *
     * @param $id ID of upload to check status of
     *
     */
    public function status($id) {
        // get the phases for this archive
        $phases = $this->Phase->find('all', [
            'conditions' => [
                'archive_id' => $id
            ],
            'order' => 'start ASC'
        ]);

        $this->set(compact('phases', 'id'));
    }
}
