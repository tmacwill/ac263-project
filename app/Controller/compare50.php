#!/usr/bin/env php
<?php

/**
 * Compare all submissions in a directory
 *
 */

require_once __DIR__ . '/ExecutionManager.php';

if ($argc < 4) {
    print "Usage: ./compare50.php archive_id path filename\n";
    exit;
}

$manager = new ExecutionManager($argv[1], $argv[2], $argv[3]);
$manager->run();
