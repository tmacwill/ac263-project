<?php

require_once __DIR__ . '/Test.php';

class DiffTest extends Test {

    // test name
    public $test = 'diff';

    /**
     * Execute the diff test
     *
     */
    public function execute() {
        $threshold = 0.01;
        shell_exec("/var/www/html/scripts/bin/diff {$this->submissions} $threshold {$this->output} {$this->filename}");
    }
}
