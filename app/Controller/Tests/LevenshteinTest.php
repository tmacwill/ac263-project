<?php

require_once __DIR__ . '/Test.php';

class LevenshteinTest extends Test {

    // test name
    public $test = 'levenshtein';

    /**
     * Execute the diff test
     *
     */
    public function execute() {
        shell_exec("/var/www/html/scripts/bin/levenshtein {$this->submissions} {$this->filename} {$this->output}");
    }
}
