<?php

require_once __DIR__ . '/../../Config/database.php';

abstract class Test {

    // individual tests implement their own execution
    abstract public function execute();

    // test name
    public $test;

    /**
     * Constructor
     *
     * @param $executable Path to test executable
     * @param $path Submissions to run test on
     *
     */
    public function __construct($archive, $submissions, $filename) {
        $this->archive = $archive;
        $this->submissions = $submissions;
        $this->filename = $filename;

        // connect to database
        $config = (new DATABASE_CONFIG())->default;
        $this->handle = new PDO("mysql:dbname=" . $config['database'] . ";host=" . $config['host'], 
            $config['login'], $config['password']);
    }

    /**
     * Run the test
     *
     */
    public function run() {
        $this->setup();
        $this->execute();
        $this->teardown();
        $this->save();
    }

    /**
     * Saves the results of a test to the database
     *
     */
    public function save() {
        // build batch insert query
        $sql = 'insert into results (archive_id, test, source, target, score) values ' . 
            implode(',', array_map(function($e) { 
                return '(' . implode(',', [$this->archive, $this->handle->quote($e->test), $this->handle->quote($e->source), 
                $this->handle->quote($e->target), isset($e->score) ? $e->score : 1]) . ')';
            }, $this->results));

        // execute query
        $statement = $this->handle->prepare($sql);
        $statement->execute();
    }
    /**
     * Set up a test environment
     *
     */
    public function setup() {
        // create file for output
        $prefix = 'compare50-result-';
        $this->output = tempnam(sys_get_temp_dir(), $prefix);
    }

    /**
     * Finish the test
     *
     */
    public function teardown() {
        // read results from output file
        $this->results = json_decode(file_get_contents($this->output));
    }
}
