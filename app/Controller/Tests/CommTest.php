<?php

require_once __DIR__ . '/Test.php';

class CommTest extends Test {

    // test name
    public $test = 'comm';

    /**
     * Execute the diff test
     *
     */
    public function execute() {
        $threshold = 0.4;
        shell_exec("/var/www/html/scripts/bin/comm {$this->submissions} $threshold {$this->output} {$this->filename}");
    }
}

