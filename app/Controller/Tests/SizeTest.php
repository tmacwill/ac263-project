<?php

require_once __DIR__ . '/Test.php';

class SizeTest extends Test {

    // test name
    public $test = 'size';

    /**
     * Execute the size test
     *
     */
    public function execute() {
        $threshold = 0.01;
        shell_exec("/var/www/html/scripts/bin/size {$this->submissions} $threshold {$this->output} {$this->filename}");
    }
}
