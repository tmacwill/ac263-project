<?php

require_once __DIR__ . '/Tests/DiffTest.php';
require_once __DIR__ . '/Tests/SizeTest.php';
require_once __DIR__ . '/Tests/CommTest.php';
require_once __DIR__ . '/Tests/LevenshteinTest.php';
require_once __DIR__ . '/Processors/WhiteSpaceProcessor.php';
require_once __DIR__ . '/Processors/StyleProcessor.php';
require_once __DIR__ . '/Processors/SortProcessor.php';

class ExecutionManager {

    // order of tests to execute
    private $tests = [
        ['test' => 'SizeTest', 'processors' => ['WhiteSpaceProcessor']],
        ['test' => 'DiffTest', 'processors' => ['StyleProcessor']],
        //['test' => 'CommTest', 'processors' => ['SortProcessor']],
        ['test' => 'LevenshteinTest', 'processors' => []]
    ];

    /**
     * Constructor
     *
     * @param $archive Archive of submissions to test
     * @param $path Path to submissions to test
     * @param $filename File to look for within submissions to test
     *
     */
    public function __construct($archive, $path, $filename) {
        $this->archive = $archive;
        $this->path = $path;
        $this->filename = $filename;
    }

    /**
     * Notify the uploading user that the tests have completed
     *
     * @param $id ID of uploaded archive
     * @param $email User's email address
     *
     */
    private function email($id, $email) {
        require_once 'PHPMailer/class.phpmailer.php';

        $body = "Hi there,\n\n" .
            "Your tests on CS50 Compare have completed! Visit http://ac263.info.tm/results/$id to view the results!\n\n" .
            "Love,\n" .
            "The CS50 Compare Bot";

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 465; 
        $mail->Username = 'compare50@gmail.com'; 
        $mail->Password = 'Compare50!';           
        $mail->SetFrom('compare50@gmail.com', 'CS50 Compare');
        $mail->Subject = 'CS50 Compare Completed!';
        $mail->Body = $body;
        $mail->AddAddress($email);
        $mail->Send();
    }

    /**
     * Run the suite of tests
     *
     */
    public function run() {
        // connect to database
        $config = (new DATABASE_CONFIG())->default;
        $handle = new PDO("mysql:dbname=" . $config['database'] . ";host=" . $config['host'], 
            $config['login'], $config['password']);

        // run preprocessors
        $run_processors = [];
        foreach ($this->tests as $test) {
            $processors = $test['processors'];
            foreach ($processors as $idx => $processor) {
                $path = $this->path;
                // need to check if this set of preprocessors has been run yet
                if (array_key_exists(implode('-', array_slice($processors, 0, $idx + 1)), $run_processors))
                    $path = $run_processors[implode('-', array_slice($processors, 0, $idx + 1))];

                // run processors that have not been run yet
                else {
                    $p = new $processor($path);
                    $path = $p->run();
                    $run_processors[implode('-', array_slice($processors, 0, $idx + 1))] = $path;
                }
            }
        }
        
        // create and run each test object
        foreach ($this->tests as $test) {
            // operate on already-processed files if test calls for that
            if (count($test['processors']) > 0)
                $path = $run_processors[implode('-', $test['processors'])];
            else
                $path = $this->path;
            
            // instantiate test class
            $t = new $test['test']($this->archive, $path, $this->filename);

            // record state of phase
            $statement = $handle->prepare('insert into phases (archive_id, test, start) values (?, ?, ?)');
            $statement->execute([$this->archive, $t->test, date('Y-m-d H:i:s')]);
            $id = $handle->lastInsertId();

            // execute test
            $t->run();

            // update test end time
            $statement = $handle->prepare('update phases set end = ? where id = ?');
            $statement->execute([date('Y-m-d H:i:s'), $id]);
        }

        // get email address of uploader
        $statement = $handle->prepare('select email from archives where id = ?');
        $statement->execute([$this->archive]);
        $row = $statement->fetch();

        // notify uploader the tests have finished
        $this->email($this->archive, $row['email']);
    }
}
