<?php

App::uses('AppController', 'Controller');

class ArchivesController extends AppController {
    /**
     * Load associated models
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();

        Controller::loadModel('Archive');
        Controller::loadModel('Phase');
        Controller::loadModel('Result');
    }

    /**
     * Handle the upload of a new archive
     *
     */
    public function add() {
        if ($this->request->is('post')) {
            // create new archive entry
            $this->Archive->create();
            $archive = $this->Archive->save([
                'email' => $this->request->data['email'],
                'timestamp' => date('Y-m-d H:i:s')
            ]);

            // process uploaded file
            $zip = new ZipArchive;
            if ($zip->open($_FILES['file']['tmp_name']) === true) {
                // determine path to unzip into
                $path = Archive::ARCHIVE_PATH . $archive['Archive']['id'];

                // unzip files
                @mkdir($path, 0711, true);
                $zip->extractTo($path);
                $zip->close();

                // start comparison job in the background
                $this->compare($archive['Archive']['id'], "$path/");
            }

            // redirect to status page
            $this->redirect("/status/{$archive['Archive']['id']}");
            exit;
        }
    }

    /**
     * Run the comparison job on a given archive
     *
     * @param $archive_id Archive to compare
     * @param $path Path to files within archive
     *
     */
    private function compare($archive_id, $path) {
        // comparison configuration
        $sample = 10;
        $threshold = 0.8;
        $processes = 4;

        // get all submissions in archive
        $submissions = glob($path . '*');
        $n = count($submissions);
        if ($sample > $n)
            $sample = $n;

        // randomly sample directories to determine files that should be present
        $viewed = [];
        $found = [];
        for ($i = 0; $i < $sample; $i++) {
            // determine a random directory we haven't seen yet
            do
                $index = rand(0, $n - 1);
            while (isset($viewed[$index]));

            // remember we have seen this submission
            $submission = $submissions[$index];
            $viewed[$index] = 1;

            // iterate over files in submission
            $files = glob($submission . '/*');
            $offset = strlen($submission) + 1;
            foreach ($files as $file) {
                $f = substr($file, $offset);
                if (!isset($found[$f]))
                    $found[$f] = 0;
                $found[$f]++;
            }
        }

        // only retain those files passing the given threshold
        $queue = [];
        foreach ($found as $file => $count)
            if ((float)$count / (float)$sample > $threshold)
                $queue[] = $file;
        $files = implode(" ", $queue);

        // kick off comparison process
        exec(__DIR__ . "/compare50.py $processes $archive_id $path $files > /dev/null 2> /dev/null &");
    }

    /**
     * View for uploading a new file
     *
     */
    public function upload() {
    }
}
