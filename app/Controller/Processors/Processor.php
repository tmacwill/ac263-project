<?php

require_once __DIR__ . '/../../Config/database.php';

abstract class Processor {

    // individual processors implement their own process function
    abstract public function process();

    // processor name
    public $processor;

    /**
     * Constructor
     *
     * @param $executable Path to processor executable
     * @param $path Submissions to run processor on
     *
     */
    public function __construct($submissions) {
        $this->submissions = $submissions;
    }

    /**
     * Run the processor
     *
     */
    public function run() {
        $this->setup();
        $this->process();
        return $this->output;
    }

    /**
     * Set up a test environment
     *
     */
    public function setup() {
        // create folder for output
        //$prefix = $this->processor . '-' . $this->folder;
        $this->output = '/var/www/files/' . uniqid() . '/';
        @mkdir($this->output);
    }
}

