<?php

require_once __DIR__ . '/Processor.php';

class CommentProcessor extends Processor {

    // processor name
    public $processor = 'comment';

    /**
     * Execute the comment processor
     *
     */
    public function process() {
        shell_exec("/var/www/html/scripts/bin/comment {$this->submissions} {$this->output}");
    }
}

