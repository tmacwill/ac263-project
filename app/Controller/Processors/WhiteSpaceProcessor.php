<?php

require_once __DIR__ . '/Processor.php';

class WhiteSpaceProcessor extends Processor {

    // processor name
    public $processor = 'white';

    /**
     * Execute the whitespace processor
     *
     */
    public function process() {
        shell_exec("/var/www/html/scripts/bin/whitespace {$this->submissions} {$this->output}");
    }
}

