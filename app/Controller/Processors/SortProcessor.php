<?php

require_once __DIR__ . '/Processor.php';

class SortProcessor extends Processor {

    // processor name
    public $processor = 'sort';

    /**
     * Execute the comment processor
     *
     */
    public function process() {
        shell_exec("/var/www/html/scripts/bin/sort_all {$this->submissions} {$this->output}");
    }
}

