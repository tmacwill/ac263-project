<?php

require_once __DIR__ . '/Processor.php';

class StyleProcessor extends Processor {

    // processor name
    public $processor = 'style';

    /**
     * Execute the comment processor
     *
     */
    public function process() {
        shell_exec("/var/www/html/scripts/bin/style_all {$this->submissions} {$this->output}");
    }
}

