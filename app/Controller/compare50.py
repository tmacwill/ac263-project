#!/usr/bin/env python

import multiprocessing
import os
import subprocess
import sys

def compare(filename):
    global processes
    global archive_id
    global path
    global compare50_path

    subprocess.call(['php', compare50_path, archive_id, path, filename])

if len(sys.argv) < 5:
    print "Usage: compare50.py processes archive_id path filenames"
    sys.exit()

processes = int(sys.argv[1])
archive_id = sys.argv[2]
path = sys.argv[3]
compare50_path = os.path.dirname(os.path.realpath(__file__)) + "/compare50.php"

pool = multiprocessing.Pool(processes)
pool.map(compare, sys.argv[4:])
