<?php

App::uses('AppModel', 'Model');

class Result extends AppModel {

    /**
     * Construct the results graph for an archive
     *
     * @param $id Archive to generate graph for
     *
     */
    public function graph($id) {
        // get the results for this archive
        $allResults = $this->find('all', [
            'conditions' => [
                'archive_id' => $id
            ],
            'order' => 'score DESC'
        ]);

        // accumulate results
        $results = [];
        foreach ($allResults as &$result) {
            // determine two students 
            $s = $result['Result']['source'];
            $t = $result['Result']['target'];

            // make sure elements exist
            if (!isset($results[$s]))
                $results[$s] = [];
            if (!isset($results[$s][$t]))
                $results[$s][$t] = 0;

            // accumulate score
            $results[$s][$t] += $result['Result']['score'];
        }
 
        // create json
        $json = [
            'nodes' => [],
            'edges' => []
        ];

        // build graph json
        $added = [];
        foreach ($results as $source => $v) {
            foreach ($v as $target => $score) {
                // avoid adding duplicate nodes
                if (!isset($added[$source])) {
                    $added[$source] = 1;

                    // calculate total score for this node
                    $total = 0;
                    foreach ($v as $s)
                        $total += $s;

                    $json['nodes'][] = [
                        'id' => $source,
                        'score' => $total
                    ];
                }

                // add edge between source and target
                $json['edges'][] = [
                    'source' => $source,
                    'target' => $target,
                    'weight' => $score
                ];
            }
        }

        return [
            'results' => $results,
            'json' => $json
        ];
    }
}
