$(function() {
    function draw(graph, threshold, layout) {
        if (threshold === undefined)
            threshold = 0;

        if (layout === undefined)
            layout = 0;

        $('#graph').empty();
        var root = document.getElementById('graph');
        grapher = sigma.init(root);

        var added = {};
        for (var i = 0; i < graph.nodes.length; i++) {
            if (parseInt(graph.nodes[i].score) >= threshold) {
                var id = graph.nodes[i].id;
                var score = graph.nodes[i].score;
                added[id] = true;

                // lerp between red and green
                var r = parseInt(score / maxScore * 255);
                var g = parseInt((1 - score / maxScore) * 255);
                var b = 0;

                // add node to graph
                grapher.addNode(graph.nodes[i].id, {
                    label: graph.nodes[i].id + ', ' + score + ' / ' + maxScore,
                    x: Math.random(),
                    y: Math.random(),
                    size: 5,
                    color: 'rgb(' + r + ', ' + g + ', ' + b + ')'
                });
            }
        }

        // add edges between nodes
        for (var i = 0; i < graph.edges.length; i++)
            if (added[graph.edges[i].source] && added[graph.edges[i].target])
                grapher.addEdge(graph.edges[i].source + ',' + graph.edges[i].target, 
                    graph.edges[i].source, graph.edges[i].target);

        // show only connected nodes upon hovering over a node
        // http://sigmajs.org/examples/hidden_nodes.html
        grapher.bind('overnodes', function(event) {
            var nodes = event.content;
            var neighbors = {};
            grapher.iterEdges(function(e) {
                if (nodes.indexOf(e.source) >= 0 || nodes.indexOf(e.target) >= 0) {
                    neighbors[e.source] = 1;
                    neighbors[e.target] = 1;
                }
            }).iterNodes(function(n) {
                if (!neighbors[n.id])
                    n.hidden = 1;
                else
                    n.hidden = 0;
            }).draw(2, 2, 2);
        }).bind('outnodes', function() {
            grapher.iterEdges(function(e) {
                e.hidden = 0;
            }).iterNodes(function(n) {
                n.hidden = 0;
            }).draw(2, 2, 2);
        });

        grapher.draw();

        if (layout == 0)
            grapher.circleLayout();
        else
            grapher.randomLayout();
    }

    /**
     * Circular layout
     * http://sigmajs.org/examples/a_plugin_example.html
     *
     */
    sigma.publicPrototype.circleLayout = function() {
        var R = 100;
        var i = 0;
        var L = this.getNodesCount();

        this.iterNodes(function(n) {
            n.x = Math.cos(Math.PI * (i++) / L) * R;
            n.y = Math.sin(Math.PI * (i++) / L) * R;
        });

        return this.position(0, 0, 1).draw();
    };

    /**
     * Random layout
     * http://sigmajs.org/examples/a_plugin_example.html
     *
     */
    sigma.publicPrototype.randomLayout = function() {
        var W = 100;
        var H = 100;

        this.iterNodes(function(n){
            n.x = W * Math.random();
            n.y = H * Math.random();
        });

        return this.position(0, 0, 1).draw();
    };

    // table plugin for full results view
    $('#tabs').tabs();
    $('#table').dataTable({
        "aaSorting": [[ 2, "desc" ]]
    });

    // compute max score
    var maxScore = 0;
    for (var i = 0; i < graph.nodes.length; i++)
        maxScore = Math.max(maxScore, graph.nodes[i].score);

    // create threshold slider
    $('#threshold-slider').slider({
        min: 0,
        max: maxScore,
        slide: function(event, ui) {
            window.threshold = ui.value;
            $('#threshold-value').text(window.threshold);
        },
        stop: function(event, ui) {
            window.threshold = ui.value;
            draw(window.graph, window.threshold, window.layout);
        }
    });

    // show diff when select is changed
    $('#source, #target').on('change', function() {
        // determine files to compare
        var source = $('#source').val();
        var target = $('#target').val();

        // get diff from server
        $.getJSON('/results/diff/' + id + '?source=' + source + '&target=' + target, function(response) {
            $('#diff').html(response.content || '<h2>Files are identical.</h2>');

            // update table text
            $('table.Differences th').eq(0).text(source);
            $('table.Differences th').eq(1).text(target);
        });
    });

    // display initial files
    $('#source').trigger('change');

    draw(window.graph);
});
