#!/bin/bash
FILES=$@
for f in $FILES
do
  echo "Processing $f file..."
  mkdir $f
  for dir in `ls`
  do
      cp $dir/$f $f/$dir-$f
  done
done

