#!/bin/bash
FILES=$1
for f in `ls $FILES`
do
  echo "Processing $f file..."
  # take action on each file. $f store current file name
  git clone $1/$f checkouts/$1/${f%%.*}
done
