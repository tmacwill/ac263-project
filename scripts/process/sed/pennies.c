/******************************************************************
* pennies.c
*
* Computer Science 50
* Zijian Wu
*
* Finds out how much money one will have if they earn x number of
* pennies the first day of the month and then twice that number
* the next day until the end of the month.
*
******************************************************************/

#include <stdio.h>
#include <cs50.h>

int
main(void)
{   
    // loop until sensible answer for number of days in month
    int a;
    do
    {
        printf("Days in month: ");
        a = GetInt();        
    }
    while (a<27 || a>31);
    
    // loop until non-negative number of pennies on first day
    double b;
    do
    {
        printf("Pennies on first day: ");
        b = GetDouble();
    }
    while (b<0);
    
    // converts input into cents
    b = b * 0.01;
    
   /*
    * adds up the total amount of money. 'b,' the amount of money earned
    * per day doubles after each day, while 'a' the day counter, diminishes
    * 'c' helps sum up the money.
    */
    
    double c;
    while (a>0)
    {
        c = c + b;
        b = b * 2;
        a = a - 1;
    }
    
    // prints answer
    printf("$%.2lf\n", c);
}
