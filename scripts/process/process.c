/*
 * process.c
 *
 * AC263 Final Project
 *
 * Runs some preprocessing on every file in a folder.
 * Example: ./rmcomm pset1 pset1-rmcomm
 */

#include "process.h"

int process(char *(*eff)(char*), int argc, char** argv) {
    if (argc < 3) {
        printf("Usage: %s indir outdir\n", argv[0]);
        return 1;
    }

    // open directory
    // directory structure is dir/student/file
    struct dirent* student_entry;
    struct dirent* file_entry;

    // XXX: WE SHOULD HAVE A STRUCT THAT KNOWS STRLEN(ARGV[1]) 
    char student_path[PATHNAME_SIZE];
    char new_student_path[PATHNAME_SIZE];
    char file_path[PATHNAME_SIZE];

    DIR* dp;
    DIR* student_dp;
    dp = opendir(argv[1]);
    if (dp == NULL) {
        printf("Failed to open directory\n");
        return 2;
    }

    // build linked list of files in given directory
    struct node* files = NULL;
    int n = 0;
    while ((student_entry = readdir(dp))) {
        // ignore hidden files
        if (student_entry->d_name[0] == '.')
            continue;

        sprintf(student_path, "%.128s%.128s", argv[1], student_entry->d_name);
        sprintf(new_student_path, "%.128s%.128s", argv[2], student_entry->d_name);

        // create new folder for student
        printf("%s\n", new_student_path);
        mkdir(new_student_path, S_IRWXU);
        
        student_dp = opendir(student_path);
        if (student_dp == NULL) {
            printf("Failed to open directory %s\n", student_path);
            continue;
        }
        while ((file_entry = readdir(student_dp))) {
            // ignore hidden files
            if (file_entry->d_name[0] == '.')
                continue;
            sprintf(file_path, "%.128s/%.128s", student_entry->d_name, file_entry->d_name);
            // add entry to linked list
            struct node* file = malloc(sizeof(struct node));
            strncpy(file->filename, file_path, FILENAME_SIZE);
            file->next = files;
            files = file;
            n++;
        }
        closedir(student_dp);
    }
    closedir(dp);

    // perform n-way comparison of file sizes
    struct stat buf1;
    struct node* i = files;
    while (i != NULL) {
        // concat filename to path
        char* f1 = malloc(strlen(i->filename) + strlen(argv[1]) + 1);
        sprintf(f1, "%s%s", argv[1], i->filename);
       
        
        char* f2 = malloc(strlen(i->filename) + strlen(argv[2]) + 1);
        sprintf(f2, "%s%s", argv[2], i->filename);
        
        // read files
        if (stat(f1, &buf1)) {
            printf("Failed to stat %s\n.", f1);
            return false;
        }

        int fd;
        char* source;
        fd = open(f1, O_RDONLY);
        if (fd < 0) {
            printf("Failed to open %s\n.", f1);
            return false;
        }
        source = mmap(0, buf1.st_size, PROT_READ, MAP_SHARED, fd, 0);

        if (source == MAP_FAILED) {
            printf("Map failed for %s\n.", f1);
            return false;
        }

        char* output = eff(source);

        FILE* outfile = fopen(f2, "w");
        fwrite(output, strlen(output), 1, outfile);
        fclose(outfile);
        
        close(fd);
        munmap(source, buf1.st_size);

        // advance outer pointer
        i = i->next;
    }
    
    return 0;
}


