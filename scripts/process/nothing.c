/*
 * rmcomments.c
 *
 * AC263 Final Project
 *
 * Removes all comments from files in a directory.
 */

#include "process.h"
#include "processors.h"

int main(int argc, char* argv[])
{
    char *(*eff)(char* source) = {&nothing};
    process(eff, argc, argv);
}
