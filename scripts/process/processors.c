#include "processors.h"

char* nothing(char* source)
{
    return source;
}

char* strip_whitespace(char* source)
{
    int len = strlen(source);
    char* stripped_text = malloc(len);
    char c1;
    int idx = 0;
    while (*source != '\0') {
        while (isspace(c1 = (unsigned char) *source++));
        stripped_text[idx++] = c1;
    }
    stripped_text[idx] = '\0';

    return stripped_text;
}
