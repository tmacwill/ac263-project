#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <json/json.h>

#define FILENAME_SIZE 128

struct node {
    struct node* next;
    char filename[FILENAME_SIZE];
};

int main(int argc, char** argv) {
    float threshold = 0.1;
    
    if (argc < 3) {
        printf("Usage: size dir [threshold] outfile\n");
        return 1;
    }

    // optional threshold as third argument
    if (argc == 4)
        threshold = atof(argv[2]);
    
    // initialize json array to hold results
    json_object *jarray = json_object_new_array();

    // open directory
    struct dirent* entry;
    DIR* dp;
    dp = opendir(argv[1]);
    if (dp == NULL) {
        printf("Failed to open directory %s\n", argv[1]);
        return 2;
    }

    // build linked list of files in given directory
    struct node* files = NULL;
    int n = 0;
    while ((entry = readdir(dp))) {
        // ignore hidden files
        if (entry->d_name[0] == '.')
            continue;

        // add entry to linked list
        struct node* file = malloc(sizeof(struct node));
        strncpy(file->filename, entry->d_name, FILENAME_SIZE);
        file->next = files;
        files = file;
        n++;
    }
    closedir(dp);

    // perform n-way comparison of file sizes
    struct stat buf1;
    struct stat buf2; 
    struct node* i = files;
    struct node* j = files;
    while (i != NULL) {
        while (j != NULL) {
            // don't compare a file to itself
            if (i != j) {
                // concat filename to path
                char* f1 = malloc(strlen(i->filename) + strlen(argv[1]) + 1);
                char* f2 = malloc(strlen(j->filename) + strlen(argv[1]) + 1);
                sprintf(f1, "%s%s", argv[1], i->filename);
                sprintf(f2, "%s%s", argv[1], j->filename);

                // read files
                if (stat(f1, &buf1)) {
                    printf("Failed to stat %s\n.", f1);
                    return false;
                }
                if (stat(f2, &buf2)) {
                    printf("Failed to stat %s\n.", f2);
                    return false;
                }

                // determine filesize similarity
                float size1 = (float)buf1.st_size;
                float size2 = (float)buf2.st_size;
                if (size1 + size1 * threshold >= size2 && size1 - size1 * threshold <= size2) {
                    //printf("%s and %s are similar!\n", i->filename, j->filename);

                    // create and add json object to array
                    json_object * jobj = json_object_new_object();
                    json_object_object_add(jobj,"test", json_object_new_string("size"));
                    json_object_object_add(jobj,"source", json_object_new_string(i->filename));
                    json_object_object_add(jobj,"target", json_object_new_string(j->filename));
                    json_object_array_add(jarray,jobj);
                }
            }

            // advance inner pointer
            j = j->next;
        }

        // advance outer pointer and reset inner pointer
        i = i->next;
        j = files;
    }

    // write file
    const char* str = json_object_to_json_string(jarray);
    FILE* outfile = fopen(argv[argc-1], "w");
    fwrite(str, strlen(str), 1, outfile);
}
