import java.io.File;
import java.io.FileInputStream;
import java.nio.channels.FileChannel;
import java.nio.MappedByteBuffer;

class NWay {
    public static void main(String[] args) throws Exception {
        int files = 2000;
        long start = System.currentTimeMillis();

        for (int i = 0; i < files; i++) {
            for (int j = 0; j < files; j++) {
                FileInputStream f1 = new FileInputStream("files/file" + i + ".c");
                FileChannel ch1 = f1.getChannel();
                MappedByteBuffer mb1 = ch1.map(FileChannel.MapMode.READ_ONLY, 0L, ch1.size());
                long file1 = 0L;
                while (mb1.hasRemaining())
                    file1 += mb1.get();

                FileInputStream f2 = new FileInputStream("files/file" + j + ".c");
                FileChannel ch2 = f2.getChannel();
                MappedByteBuffer mb2 = ch2.map(FileChannel.MapMode.READ_ONLY, 0L, ch2.size());
                long file2 = 0L;
                while (mb2.hasRemaining())
                    file2 += mb2.get();

                if (file1 == file2) {}

                f1.close();
                f2.close();
            }
        }

        long end = System.currentTimeMillis();

        System.out.println("Time taken: " + ((end - start) / 1000));
    }
}
