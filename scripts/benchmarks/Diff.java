import java.io.File;
import java.io.FileInputStream;
import java.nio.channels.FileChannel;
import java.nio.MappedByteBuffer;

class Diff {
    public static void main(String[] args) throws Exception {
        int files = 10000;
        long start = System.currentTimeMillis();

        FileInputStream f = new FileInputStream("files/source0.c");
        FileChannel ch = f.getChannel();
        MappedByteBuffer mb = ch.map(FileChannel.MapMode.READ_ONLY, 0L, ch.size());
        long source = 0L;
        while (mb.hasRemaining())
            source += mb.get();

        for (int i = 0; i < files; i++) {
            FileInputStream fi = new FileInputStream("files/file" + i + ".c");
            FileChannel chi = fi.getChannel();
            MappedByteBuffer mbi = chi.map(FileChannel.MapMode.READ_ONLY, 0L, chi.size());
            long file = 0L;
            while (mbi.hasRemaining())
                file += mbi.get();

            if (source == file) {}

            fi.close();
        }

        long end = System.currentTimeMillis();

        System.out.println("Time taken: " + ((end - start) / 1000));
    }
}
