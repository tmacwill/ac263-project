#!/usr/bin/env bash

SIZES=(5 10 50 100 200 300 500)
BINS=("size" "diff_whitespace" "diff")

for size in ${SIZES[*]}
do
    for bin in ${BINS[*]}
    do
        echo "testing $bin on $size files"
        (time /root/html/scripts/bin/$bin /mnt/psets/cs50/2011/checkouts/pset1_sizes/pset1_$size/ results/out.txt) &> results/$bin-$size.txt
    done
done

SLOW_BINS=("comm")

for size in ${SIZES[@]:0:4}
do
    for bin in ${SLOW_BINS[*]}
    do
        echo "testing $bin on $size files"
        (time /root/html/scripts/bin/$bin /mnt/psets/cs50/2011/checkouts/pset1_sizes/pset1_$size/ results/out.txt) &> results/$bin-$size.txt
    done
done
