#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv) {
    long long start = time(NULL);

    int files = 4000;
    struct stat buf1;
    struct stat buf2; 
    char filename[100];
    
    for (register int i = 0; i < files; i++) {
        for (register int j = 0; j < files; j++) {
            sprintf(filename, "files/file%d.c", i);
            if (stat(filename, &buf1))
                return false;

            int fd1 = open(filename, O_RDONLY);
            if (fd1 < 0)
                return false;

            char* file1 = mmap(0, buf1.st_size, PROT_READ, MAP_SHARED, fd1, 0);
            if (file1 == MAP_FAILED)
                return false;

            sprintf(filename, "files/file%d.c", j);
            if (stat(filename, &buf2))
                return false;

            int fd2 = open(filename, O_RDONLY);
            if (fd2 < 0)
                return false;

            char* file2 = mmap(0, buf2.st_size, PROT_READ, MAP_SHARED, fd1, 0);
            if (file2 == MAP_FAILED)
                return false;

            if (strcmp(file1, file2) == 0) {}

            close(fd1);
            close(fd2);
            munmap(file1, buf1.st_size);
            munmap(file2, buf2.st_size);
        }
    }

    long long end = time(NULL);

    printf("Time taken: %lld\n", end - start);
    return 0;
}
