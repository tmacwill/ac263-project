import time

files = 100000;

start = time.time();

n = 0;
s = open("files/source0.c", "r");
source = ''.join(s.readlines())
for i in xrange(files):
    f = open("files/file%d.c" % (i))
    content = ''.join(f.readlines())
    if (source != f):
        pass

    f.close()

end = time.time();

time = end - start;
print "Time taken: %d" % (time)
