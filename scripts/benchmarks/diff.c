#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <json/json.h>

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Usage: diff outfile\n");
        return 1;
    }

    json_object *jarray = json_object_new_array();

    long long start = time(NULL);
    int files = 100000;

    struct stat buf; 
    if (stat("files/source0.c", &buf))
        return false;
    char* source_name = "files/source0.c";
    int fd = open(source_name, O_RDONLY);
    if (fd < 0)
        return false;

    char* source = mmap(0, buf.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (source == MAP_FAILED) 
        return false;

    for (register int i = 0; i < files; i++) {
        char filename[100];
        sprintf(filename, "files/file%d.c", i);

        int fdi = open(filename, O_RDONLY);
        if (fdi < 0)
            return false;

        char* file = mmap(0, buf.st_size, PROT_READ, MAP_SHARED, fdi, 0);
        if (file == MAP_FAILED)
            return false;

        if (strcmp(source, file) == 0) {
            printf("%s and %s are similar!\n", source_name, filename);
            json_object * jobj = json_object_new_object();
            json_object_object_add(jobj,"test", json_object_new_string("diff"));
            json_object_object_add(jobj,"source", json_object_new_string(source_name));
            json_object_object_add(jobj,"target", json_object_new_string(filename));
            json_object_array_add(jarray,jobj);

            // XXX DO I HAVE TO FREE THESE THINGS?

        }

        close(fdi);
        munmap(file, buf.st_size);
    }

    long long end = time(NULL);

    printf("Time taken: %d\n", ((end - start) / 1000));

    char* str = json_object_to_json_string(jarray);
    FILE* outfile = fopen(argv[1], "w");
    fwrite(str, strlen(str), 1, outfile);
    fclose(outfile);
}
