import time

files = 2000

start = time.time();

for i in xrange(files):
    for j in xrange(files):
        f1 = open("files/file%d.c" % (i), "r")
        f2 = open("files/file%d.c" % (j), "r")
        content1 = ''.join(f1.readlines())
        content2 = ''.join(f2.readlines())
        if (content1 != content2):
            pass

        f1.close()
        f2.close()

end = time.time();

time = end - start;
print "Time taken: %d" % (time)
