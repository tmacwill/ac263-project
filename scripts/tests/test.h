#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <json/json.h>

#define FILENAME_SIZE 128
#define PATHNAME_SIZE 256
struct node {
    struct node* next;
    char filename[FILENAME_SIZE];
};

/* test takes in an array of functions, and runs those functions over the files
 * defined by argv, building a json array, and writing to an output file defined by 
 * argv if "open_files", the functions in effs require the file to be open. 
 * */

int test(void (*effs[])(char*, char*, float, char*, char*, char*, json_object*), int num_effs, int open_files, int argc, char** argv);
