/*
 * tests.c
 *
 * AC263 Final Project
 *
 * Implements various C tests for comparing source code.
 */

#define _XOPEN_SOURCE 500
#include <ctype.h>

#include "tests.h"

void json_add(json_object* jarray, char* test, char* source_name, char* target_name, float score);
char* system_str(char* command, int output_size);

/*
 * Performs byte-by-byte comparison using strcmp
 */
void diff(char* source, char* target, float  threshold, char* folder, char* source_name,
          char* target_name, json_object* jarray) {
    if (strcmp(source, target) == 0) {
        json_add(jarray, "diff", source_name, target_name, 1);
    }
}

/*
 * Finds number of lines that are unique to file1
 */
void comm(char* source, char* target, float threshold, char* folder, char* source_name,
          char* target_name, json_object* jarray) {
    char command[1024];
    sprintf(command, "/usr/bin/bash -c \"comm -2 -3 %s%s %s%s | wc -l\"", folder, source_name, folder, target_name);
    float score;
    if ((score = atoi(system_str(command, 1000))) > 0) {
        if (1.0/score > threshold) {
            json_add(jarray, "comm", source_name, target_name, 1/score);
        }
    }
}

/*
 * Performs byte-by-byte comparison, skipping whitespace
 */
void diff_whitespace(char* source, char* target, float threshold, char* folder, char* source_name,
                     char* target_name, json_object* jarray) {
    unsigned char c1, c2;
    do
    {
        while (isspace(c1 = (unsigned char) *source++));
        while (isspace(c2 = (unsigned char) *target++));
        if (c1 == '\0')
            break;
    }
    while (c1 == c2);
    if (c1 == c2)
    {
        json_add(jarray, "diff", source_name, target_name, 1);
    }
}

/*
 * Create and add json object to array
 */
void json_add(json_object* jarray, char* test, char* source_name, char* target_name, float score)
{
    json_object* jobj = json_object_new_object();
    json_object_object_add(jobj, "test", json_object_new_string(test));
    json_object_object_add(jobj, "source", json_object_new_string(source_name));
    json_object_object_add(jobj, "target", json_object_new_string(target_name));
    json_object_object_add(jobj, "score", json_object_new_double(score));
    json_object_array_add(jarray,jobj);
}

/*
 * Run a system call, and return the output in a string
 * (modified from http://stackoverflow.com/questions/646241/c-run-a-system-command-and-get-output)
 */
char* system_str(char* command, int output_size) {
    FILE *fp;
    char* result = malloc(output_size);

    /* Open the command for reading. */
    fp = popen(command, "r");
    if (fp == NULL) {
        return NULL;
    }

    /* Read the output a line at a time - output it. */
    fgets(result, sizeof(result)-1, fp);
    
    /* close */
    pclose(fp);
    return result;
}
