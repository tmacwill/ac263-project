#include <json/json.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
void diff(char* source, char* target, float threshold, char* folder, char* source_name, char* target_name, json_object* jarrary);
void diff_whitespace(char* source, char* target, float threshold, char* folder, char* source_name, char* target_name, json_object* jarrary);
void comm(char* source, char* target, float threshold, char* folder, char* source_name, char* target_name, json_object* jarrary);
