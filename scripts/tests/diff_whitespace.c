/*
 * diff_whitespace.c
 *
 * AC263 Final Project
 *
 * Uses diff_whitespace test to compare files.
 */

#include "test.h"
#include "tests.h"

int main(int argc, char* argv[])
{
    void (*effs[])(char* source, char* target, char* source_name,
                   char* target_name, json_object* jarrary) = {&diff_whitespace};
    test(effs, 1, 1, argc, argv);
}
