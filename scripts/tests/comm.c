/*
 * comm.c
 *
 * AC263 Final Project
 *
 * Uses comm test to compare files.
 */

#include "test.h"
#include "tests.h"

int main(int argc, char* argv[])
{
    void (*effs[])(char* source, char* target, float threshold, char* folder, char* source_name,
                   char* target_name, json_object* jarrary) = {&comm};
    test(effs, 1, 0, argc, argv);
}
